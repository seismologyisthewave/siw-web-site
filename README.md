# Seismology is the Wave Web-Site

## Este es el sitio web de SIW, estaremos actualiziando con nuestro contenido de divulgación y humor científico mas relevante

## Como platilla utilizamos :

>A technology-minded theme for Hugo based on VMware's open-source [Clarity Design System](https://clarity.design/) featuring rich code support, dark/light mode, mobile support, and much more. See [a live demo at __neonmirrors.net__](https://neonmirrors.net/).

> ![Clarity Hugo Theme](https://github.com/chipzoller/hugo-clarity/blob/master/images/screenshot.png)

# El sitio fue desarrollado por el equipo de SIW:
* Manu
* Mike
* Luis
* Leo
* Diego
* Daniel 

## ***Este sitio no tiene fines de lucro, solo pretende hacer divulgación cientifica en temas de Sismología***

## Atentamente: *Equipo de SIW*
