+++
author = "SIW-Crew"
title = "Tierra Fest 2021"
date ="2021-03-10"
tags = [ "Tierra Fest 2021","Wikicarrera"] 
thumbnail = "images/div/TierraFest.jpeg"
+++

# **Seismology is the wave** y **Planeteando** los invitamos al TierraFest 2021.

![Tierra Fest 2021](/images/div/TierraFest.jpeg)

# Los invitamos a la **WikiCarrera de la Tierra** 
### Próximamente publicaremos detalles de bases, inscripción y premios 

![WikiCarrera](/images/div/WikiCarrera.png)