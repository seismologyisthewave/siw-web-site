+++
author = "SIW-Crew"
title = "Convocatoria a la WikiCarrera de la Tierra 2021"
date = "2021-03-17"
tags = [ "Tierra Fest 2021","Wikicarrera"] 
thumbnail = "images/default.jpg"
+++


<iframe width='100%' height='100%' frameborder='0' src="https://docs.google.com/document/d/e/2PACX-1vTCwsSJzCZw90mLnRCGo6gZzrL4H4AuwH0MpiZ9klF67BLL3A3_JG48sWneJxpYZw/pub?embedded=true"></iframe>

###              [Descarga la convocatoria en PDF](/docs/WikiCarrera_convocatoria_210317.pdf)
<!-- 
|Seismology Is The Wave| Planeteando|
|--|--| 
|<img src="/logos/logo1.png" width="200" height="200">|<img src="/logos/planeteando.jpeg" width="250" height="300">|
  
### Convocan a la WikiCarrera de la Tierra como parte del Tierra-Fest 2021  

##### Como parte de los eventos que conforman el Tierra-fest 2021, Planeteando y Seismology is the Wave te hacen una cordial invitación para participar en la WikiCarrera de la Tierra, la cual se llevará a cabo el día miércoles 21 de abril del 2021 a las 16:00 hrs (hora del centro de México). La dinámica pretende resaltar tus conocimientos y habilidades en temas de Ciencias de la Tierra y consiste en  navegar  desde una  página de Wikipedia de un tema a la de otro tema en el contexto de las Ciencias de la Tierra. El registro para participar y la descripción de la dinámica se detallan en los siguientes puntos:
* ##### **Primero.** Se convoca a alumnos de cualquier nivel, profesores, trabajadores y en general a cualquier persona interesada en Ciencias de la Tierra.

* ##### **Segundo.** El registro de participantes del concurso se realizará a través del siguiente [formulario](https://forms.gle/mYo9DgSjRQbPVjWo6)
	> LIMITADO A 50 PARTICIPANTES. En caso de haber registros duplicados se tomará el primero como válido.
* ##### **Tercero.** El concurso se realizará bajo la siguiente dinámica (siempre que se tengan cinco o más participantes inscritos):
	- El evento será transmitido en vivo a través de las cuentas de Facebook de [Planetenado](https://www.facebook.com/Planetea/) y [Seismology is the Wave](https://www.facebook.com/SeismologyIsTheWave).
	- Las/Los participantes inscritos estarán (en toda la competencia) en una reunión de Zoom desde la que se organizará todo el evento (el link se proporcionará a los participantes inscritos el día de la competencia).
	- Las/Los participantes inscritos jugarán una primera ronda de eliminación conformada por diez preguntas de opción múltiple utilizando la plataforma Kahoot (el código se proporcionará a los participantes inscritos el día de la competencia), sugerimos descargarla en un dispositivo móvil, aunque también es posible usarla desde el navegador del teléfono, de la tablet o del dispositivo de cómputo de su preferencia ([https://kahoot.it](https://kahoot.it/); no se requiere registro en la plataforma).
	- Cada pregunta contará con cuatro posibles respuestas y cada respuesta incluirá un número determinado de palabras, las cuales definirán cuatro “posibles” caminos para llegar de una página a otra dentro de la plataforma Wikipedia. Sin embargo, sólo una de las opciones tendrá la secuencia correcta. Las/Los participantes que contesten de manera correcta y lo más rápido posible obtendrán mayor puntuación. Al final de cada pregunta se mostrará que efectivamente la respuesta correcta era un camino existente dentro de Wikipedia.
	- Al terminar la primera ronda, se tendrá un grupo de cuatro semifinalistas (determinado por los primeros cuatro lugares arrojados por el marcador de Kahoot) que participarán en dos rondas de eliminación directa. En la ronda A se enfrentarán el primer lugar contra el cuarto, mientras que en la ronda B harán lo propio el segundo y el tercero. Las semifinales se jugarán a un máximo de tres preguntas en donde las/los participantes deberán encontrar una ruta para llegar de una página a otra, en vivo, ambas con temas relacionados con las Ciencias de la Tierra. Quien lo haga en menos tiempo ganará un punto. El primero de los contendientes que llegue a dos puntos en cada semifinal ganará su eliminatoria y avanzará a la final.
	- Los ganadores de las rondas A y B se enfrentarán en la final del certamen, la cual seguirá el mismo formato de las semifinales, con la variante de que ahora tendrán que llegar de un tema aleatorio o de cultura general a un tema de Ciencias de la Tierra o viceversa. La persona que lo haga primero ganará un punto. Quien obtenga primero dos puntos será campeón de la WikiCarrera de la Tierra 2021.
* ##### **Cuarto.** En caso de que se tengan cuatro o menos participantes inscritos, se consideran los siguientes casos:
	- Si hay cuatro participantes: semifinales de manera directa en donde se jugará a un máximo de cinco preguntas. Quien llegue primero a tres puntos pasará a la final donde se aplicará este mismo formato.
	- Si hay tres participantes: se hará una eliminación con un Kahoot de cinco preguntas, en cuyo caso los dos primeros lugares jugarán la final en donde se jugará a un máximo de cinco preguntas. Quien llegue primero a tres puntos será el campeón.
	- Si hay dos participantes: final de manera directa. Quien llegue primero a cinco puntos, de nueve posibles, será el campeón de la WikiCarrera de la Tierra 2021.
	- Si hay un participante: será declarado ganador y participará en un Kahoot de exhibición con los equipos de Planeteando y Seismology is the Wave.
* ##### **Quinto.** Se premiará al ganador de la ronda final. Si quieres conocer acerca de los premios de invitamos a visitar [Premios Planeteando](https://planeteando.org/premios-del-tierrafest/).
* ##### **Sexto.** La decisión de los jueces del concurso será inapelable.
* ##### **Séptimo.** Los competidores manifiestan estar de acuerdo con las bases del concurso al completar su registro como participantes, lo cual incluye la posibilidad de compartir la pantalla de su navegador durante la transmisión en vivo en caso de ser semifinalistas y/o finalistas. Esto es necesario porque se narrará, en tiempo real, la secuencia de páginas de Wikipedia que se siguió para cumplir con el objetivo del concurso.
* ##### **Octavo.** Cualquier imprevisto será resuelto por el equipo organizador de la WikiCarrera de la Tierra (Seismology is the Wave y Planeteando) antes o incluso al momento de la transmisión.
* ##### **Noveno.** La premiación se llevará a cabo el día sábado 24 de abril del 2021.


#####  Te invitamos a participar e invitar a tus amigos. ¡Nos vamos a divertir!

Atentamente,  Seismology is the Wave y Planeteando -->
