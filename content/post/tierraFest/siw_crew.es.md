+++
author = "SIW-Crew"
title = "SIW crew en la WikiCarrera del #TierraFest2021"
date ="2021-04-12"
tags = [ "Tierra Fest 2021","Wikicarrera"] 
thumbnail = "images/default.jpg"
+++


# ¿Quién estará en la transmisión de la WikiCarrera del **#TierraFest**?

### Él es Diego ([@DiegoEspKus](https://twitter.com/DiegoEspKus)), estudia Ingeniería Geofísica y le gusta la música. Por ahí cuentan que es un músico frustrado. 🤫  
### Será moderador de la WikiCarrera.  
<img src="https://drive.google.com/uc?export=view&id=198V7X2orrhOebf-5vubjv7E3FBdnXocw" alt="Diego" width="640" height="360">

### Ella es Leo ([@Liem211](https://twitter.com/Liem211)), Maestra en Ciencias de la Tierra y estudiante de doctorado. Le apasiona la sismología, la vulcanología y la historia.  
### Será moderadora en la WikiCarrera.
<img src="https://drive.google.com/uc?export=view&id=19bK6RZo99TbHlOr-5X8WqXGmDihaFByq" alt="Leo" width="640" height="360">

### Él es Luis ([@imluisdaniel](https://twitter.com/imluisdaniel)), es Geofísico y analista del Sismologico. Le gusta la sismología, la música y los deportes.  
### Será moderador y juez en la WikiCarrera. 

<img src="https://drive.google.com/uc?export=view&id=19KODvnezVJ5wcev0OBCJ5HDBTnWAvGfr" alt="Luis" width="640" height="360">

### Él es Manu ([@ManuAguilar411](https://twitter.com/ManuAguilar411)), estudia el doctorado en Ciencias de la Tierra, desde niño ama la sismología y es un Skywalker.  
### Será narrador en la WikiCarrera, ***tiembla Martinoli***.
<img src="https://drive.google.com/uc?export=view&id=19GeCKhTJAME8dkaRKQ4NMYS_fkBanHtD" alt="Manu" width="640" height="360">

### Él es Mike ([@MiguelRodDom](https://twitter.com/MiguelRodDom)), estudia el posgrado en Ciencias de la Tierra y le late la estructura sísmica. Es barista y entrenador Pokémon frustrado.  
### Será moderador y juez.
<img src="https://drive.google.com/uc?export=view&id=19RM8S0nuLI6CTQwk-egNFUMWdvyjF1Mj" alt="Mike" width="640" height="360">


### Él es Raúl ([@rdcoronaf](https://twitter.com/rdcoronaf)), está en el posgrado de Ciencias de la Tierra, dice que también tiembla en la Tierra Media. 
### Será narrador y juez en la WikiCarrera, ***tiembla Dr. García***. 🤫
<img src="https://drive.google.com/uc?export=view&id=19QfA3wjwn7T1Tv8wbtC9mHQnW2k5B5Ho" alt="Raúl" width="640" height="360">
