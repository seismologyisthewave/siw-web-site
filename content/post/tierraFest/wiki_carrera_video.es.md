+++
author = "SIW-Crew"
title = "Video de la Wiki Carrera"
date ="2021-04-21"
tags = [ "Tierra Fest 2021","Wikicarrera"] 
thumbnail = "images/div/WikiCarrera.png"
+++

# Hola amigos, aqui les dejamos la repetición de la Wiki Carrera de la Tierra

<iframe width="640" height="360" src="https://www.youtube.com/embed/u-8lY_sYTuM?start=222&end=3531" title="TierraFest2021-Wiki Carrera" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Si quieren ver el resto de los eventos del TierraFest 2021 vayan a la página de [Youtube de Planeteando](https://www.youtube.com/channel/UCgV2HC2599REwibgUWIKEjA)