+++
author = "SIW-Crew"
title = "¿Dónde te agarró el temblor?"
date ="2020-07-07"
tags = [ "memes","Sismología", "Humor científico"] 
thumbnail = "images/default.jpg"
+++

# Y a ustedes ¿Donde los agarro el temblor?


## Recuerden amig@s, los sismómetros son los instrumentos que se suelen utilizar para medir el movimiento ocasionado por el paso de las ondas sísmicas.

## *#SeismologyIsTheWave*

<figure class="video_container">
  <video controls="true" allowfullscreen="true" >
    <source src="/images/memes/02_Osos.mp4" type="video/mp4">
  </video>
</figure>
