+++
author = "SIW-Crew"
title = "Convocatoria a Primer concurso de calaveritas literarias sobre sismología"
date = "2021-09-30"
tags = [ "sismología","SIW","Convocatoria", "Día de Muertos"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1WZ3OFMhD61SdkiyenxWRdw0ZJrE4Rrm1"
+++

<!-- <iframe width='100%' height='100%' frameborder='0' src="https://docs.google.com/document/d/1WBB6zodruGIIEDX6x0SSS90kFmKSbzDOHQvpfrljFDY/pub?embedded=true"></iframe> -->

## **Seismology Is The Wave**
### Convocatoria al ***“Primer concurso de calaveritas literarias sobre sismología”***

### [Descarga la convocatoria](https://drive.google.com/file/d/1pVY4wLnd_Cp3HZI6k3CaCZppuH8LGoS9/view)

Como parte de las celebraciones correspondientes con el día de muertos en México, Seismology is the Wave te hace una cordial invitación para participar en el “Primer concurso de calaveritas literarias sobre sismología”. La dinámica pretende resaltar tu creatividad y habilidad para escribir calaveritas relacionadas con el equipo de Seismology is the Wave y/o la sismología. Las bases de la convocatoria se detallan en los siguientes puntos:

1. Primero. Se convoca a cualquier persona interesada en la sismología, ya sean alumnos de cualquier nivel, profesores y/o trabajadores 
1. Segundo. Cada participante deberá preparar su calaverita en formato PDF. Las especificaciones del texto son las siguiente:
	+ Titulo de la calaverita y nombre completo del autor
	+ El tipo de letra será Arial de tamaño 12 o superior.
	+ El texto deberá estar centrado y solo podrá incluir una columna.
	+ La longitud máxima será una cuartilla.
1. Tercero. El archivo deberá ser enviado  al correo [seismologyisthewave@gmail.com](mailto:seismologyisthewave@gmail.com) a más tardar el **miércoles 20 de octubre del 2021 a las 23:59 horas**. El asunto será Calaverita literaria SIW - 2021. Como parte del cuerpo del correo, incluirá una fecha especial en su vida (por ejemplo, su cumpleaños, el cumpleaños de algún ser querido, su aniversario, etc.), ya que esta información se usará para la premiación (no se publicará en redes sociales).
1. Cuarto. Se premiarán dos calaveritas. Para elegir a la primera, el viernes 29 de octubre del 2021 se publicarán todas las calaveritas en nuestro [Facebook](https://www.facebook.com/SeismologyIsTheWave). Los usuarios de dicha red social podrán reaccionar a cada una de ellas; aquella que reciba más reacciones hasta antes del lunes 01 de noviembre del 2021 a las 21:00 horas será la ganadora elegida por el público. Por otro lado, el [equipo de Seismology is the wave](/post/about-us/) (Leonarda, Jimena, Miguel, Luis, Raúl, Diego y Manuel) leerá y elegirá la segunda calaverita premiada según su criterio.Puede darse el caso de que ambas decisiones coincidan, en cuyo caso únicamente se premiará una calaverita.
1. Quinto.  Se premiará la creatividad y originalidad siempre en un contexto de cordialidad. Las calaveritas que incluyan faltas de respeto o que no hayan considerado los puntos Segundo y Tercero en su elaboración no se tomarán en cuenta.
1. Sexto. Las ganadoras o los ganadores de la dinámica recibirán una imagen personalizada con el sismograma del evento de mayor magnitud (en el mundo) registrado en la fecha especial que nos hayan indicado por correo (es importante que incluya día, mes y año). Además, tendrán la posibilidad de participar en un live con nosotros, a través de nuestra plataforma de [Instagram](https://www.instagram.com/seismologyisthewave/) en el que podrán contarnos cómo hicieron su calaverita y algunas experiencias suyas sobre sismología.
1. Séptimo. Las decisiones sobre los ganadores que se tomen tanto a partir de reacciones en Facebook como por parte del equipo de Seismology is the wave serán inapelables.
1. Octavo. Cualquier imprevisto será resuelto por el equipo de Seismology is the wave.
1. Noveno. Las ganadoras o los ganadores se anunciarán el ***02 de noviembre del 2021 a las 12:00 horas***.

#### **Te invitamos a participar e invitar a tus amigas y amigos. ¡Nos vamos a divertir!**


<div align="center"><h3>Attentamente Seismology is the Wave</h3></div>
<div align="center"><img src="/logos/logo1.png" alt="SIW" width="100" height="100"></div>





