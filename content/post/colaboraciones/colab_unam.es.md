+++
author = "SIW-Crew"
title = "La UNAM nos compartió"
date ="2020-12-02"

+++

## Fuimos mencionados por las cuentas oficiales de la Universidad Nacional Autónoma de México

<blockquote class="twitter-tweet"><p lang="und" dir="ltr"><a href="https://twitter.com/hashtag/DosisDeHumor?src=hash&amp;ref_src=twsrc%5Etfw">#DosisDeHumor</a> por <a href="https://twitter.com/SeismoIsTheWave?ref_src=twsrc%5Etfw">@SeismoIsTheWave</a>.<a href="https://twitter.com/hashtag/PontePumaPonte?src=hash&amp;ref_src=twsrc%5Etfw">#PontePumaPonte</a>😷 <a href="https://t.co/4CBhVlVt2x">pic.twitter.com/4CBhVlVt2x</a></p>&mdash; UNAM (@UNAM_MX) <a href="https://twitter.com/UNAM_MX/status/1334180588706643968?ref_src=twsrc%5Etfw">December 2, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

### Sigan a UNAM en sus redes sociales:
| <img src="/images/icons8-twitter.gif" width="100" height="100"> [@UNAM_MX](https://twitter.com/UNAM_MX)| <img src="/images/fb-logo.png" width="100" height="100"> [UNAM Universidad Nacional Autónoma de México](https://www.facebook.com/UNAM.MX.Oficial)|
|--|--|


___
---
