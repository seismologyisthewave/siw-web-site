+++
author = "SIW-Crew"
title = "Colaboramos con Cultura UNAM"
date ="2020-09-19"

+++


## Junto con [Cultura UNAM](https://cultura.unam.mx/) realizamos una infografía donde hablamos de los principales mitos alrededor de los sismos.


{{< twitter_simple 1307364514007068674 >}}

![Mitos sobre los Sismos](/images/div/CulturaUNAM.jpg "Cutura UNMA ft SIW")

### Sigan a Cultura UNAM en sus redes sociales:
| <img src="/images/icons8-twitter.gif" width="100" height="100"> [@CulturaUNAM](https://twitter.com/CulturaUNAM)| <img src="/images/fb-logo.png" width="100" height="100"> [Cultura UNAM](https://www.facebook.com/Cultura.UNAM.pagina)|
|--|--|

---
---

