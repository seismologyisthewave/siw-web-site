+++
author = "SIW-Crew"
title = "Podcast con SURSA-UNAM"
date ="2021-04-23"

+++

## Participamos en el **#Podcast** **#CuéntameTuRiesgo** organizado por [El Seminario Universitario de Riesgos Socioambientales](https://twitter.com/SURSA_UNAM) (SURSA) de la UNAM, en donde platicamos sobre nuestra iniciativa, un poco de los que integramos *Seismology is the Wave* y damos algunas sugerencias para mantenernos bien informados sobre temas de sismología.

### Aqui pueden escuchar el #Podcast  👇👇👇
### **SURSA - UNAM. Cuéntame tu riesgo: Ciencia tras bambalinas.**  
### **Episodio 3 La comunicación pública de la Sismología.**

<iframe src="https://anchor.fm/naxhelli-ruiz/embed/episodes/Episodio-3--La-comunicacin-pblica-de-la-Sismologa-evgsad/a-a5bedm8" 
height="150px" width="600px" frameborder="0" scrolling="no"></iframe>

### O lo pueden escuchar directamente en los canales oficiales de SURSA:
* [Anchor](https://anchor.fm/naxhelli-ruiz/episodes/Episodio-3--La-comunicacin-pblica-de-la-Sismologa-evgsad)

* [Spotify](https://open.spotify.com/show/6e92k59dgtp6qXvTzybl1j)

___
---


### Sigan a SURSA en sus redes sociales:
| <img src="/images/icons8-twitter.gif" width="100" height="100"> [@SURSA_UNAM](https://twitter.com/SURSA_UNAM)| <img src="/images/fb-logo.png" width="100" height="100"> [SURSA - UNAM](https://www.facebook.com/UNAM.SURSA/)|
|--|--|