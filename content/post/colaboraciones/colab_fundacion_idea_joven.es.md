+++
author = "SIW-Crew"
title = "Conferencia con Fundación IDEA JOVEN - México"
date ="2021-10-03"
tags = [ "Colaboracion", "Sismología", "SIW" ] 
thumbnail = "images/div/idea_joven.jpg"
+++

## **𝗦𝗶𝘀𝗺𝗼𝗹𝗼𝗴𝗶́𝗮, 𝘂𝗻𝗮 𝗰𝗶𝗲𝗻𝗰𝗶𝗮 𝗷𝗼𝘃𝗲𝗻**

#### Participamos en una mesa redonda con [Fundación IDEA JOVEN - México](https://www.facebook.com/FundacionIDEAJOVENMexico/)#### 

#### Fue una charla muy interesante, no se la pierdan


<iframe src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2FFundacionIDEAJOVENMexico%2Fvideos%2F457545775508272%2F&show_text=true&width=560&t=150" width="1000" height="1000" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>

## **#SeismologyIsTheWave**
## **#LaSismologíaEsLaOnda**

