+++
author = "SIW-Crew"
title = "¿Con qué onda te identificas? "
date ="2020-05-07"
tags = [ "memes","Sismología", "Humor científico"] 
thumbnail = "images/default.jpg"
+++

# ¿Con qué onda te identificas?

![Tipos de Onda](/images/memes/Tipos_de_ondas.png "Humor científico")

## *#SismoEncuesta*
## *#SeismicWar*
## *#SeismologyIsTheWave*
[¹]Fuentes: **Figuras modificadas de Shearer, 1999)**