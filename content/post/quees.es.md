+++
author = "SIW-Crew"
title = "¿Qué es Seismology is the Wave?"
date = "2020-04-17"
tags = [ "Sismología", "SIEW"] 
thumbnail = "images/poster/SIW_Estadisticas_2021.png"
+++

### La ciencia debe dejar de verse como algo ajeno a la vida cotidiana para conocerla, entenderla y sobre todo saber que no es aburrida. En consecuencia, debe ser accesible para todo tipo de público, mediante el uso de un lenguaje no técnico, que puede incluir desde memes hasta cápsulas informativas. Las redes sociales, dada su versatilidad, manejo de información y alcance, han demostrado ser una herramienta eficiente para transmitir conocimiento al público en general y grupos específicos, ya que son de fácil acceso. A través de la iniciativa ”Seismology is the Wave”, estamos llevando la divulgación de la sismología e incentivando a las nuevas generaciones a darle seguimiento, invitando a los colegas a extrapolarlo al resto de las Ciencias de la Tierra. 

---
---

![Divulgacion Científica](/images/poster/SIW_Divulgacion_2021.png "Divulgación Científica")

![Sismicidad Histórica](/images/poster/SIW_Historica_2021.png "Sismicidad Histórica")

![Humor Científico](/images/poster/SIW_Memes_2021.png "Humor Científico")

![Estadísticas](/images/poster/SIW_Estadisticas_2021.png "Estadísticas")

---
---

# Twitter Fijo
{{< twitter_simple 1329083316578910211 >}}


<div class="views">
    <span class="views">
        <img src="https://visitor-badge.glitch.me/badge?page_id={{ .Permalink }}" alt="Views"/>
    </span>
</div>