+++
author = "SIW-Crew"
title = "Magnitud de Coda"
date ="2021-03-29"
tags = [ "Magnitud", "Sismología", "Coda"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1TxwRnptH5IlQ-9QrkTj3IcqD5-3ZRquG"
+++


### Existen diferentes escalas para determinar la magnitud de un sismo. Una de ellas es la escala de magnitud de coda, la cual depende principalmente de la duración del registro y la distancia entre la estación y el epicentro. En consecuencia, esta es funcional para determinar la magnitud de sismos hasta aproximadamente 4.5.

<img src="https://drive.google.com/uc?export=view&id=1TxwRnptH5IlQ-9QrkTj3IcqD5-3ZRquG" alt="La magnitud de Coda 😟" width="640" height="360">

### Por otro lado, algunas de las escalas sufren un efecto conocido como **[spoiler alert!]** saturación, es decir, a partir de un valor dado la escala subestima la magnitud real del sismo, lo cual también las haría ineficientes para sismos grandes. 

### Sin embargo, esto es tema de otro post... 🤓


### **#SeismologyIsTheWave**  

### **#LaSismologíaEsLaOnda**
