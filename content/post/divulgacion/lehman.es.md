+++
author = "SIW-Crew"
title = "Discontinuidad de Lehmann"
date ="2021-03-08"
tags = [ "Lehman", "Sismología", "Discontinuidad sismica" ] 
thumbnail = "images/div/IngeLehmann.jpg"
+++

# *¿Sabían que antes se creía que el núcleo de la Tierra era totalmente líquido?*
## ¿Sabían que en 1936 se descubrió que el núcleo estaba dividido en dos y que el núcleo interno era sólido?
## ¿Sabían que esté descubrimiento lo realizó la sismóloga danesa **#IngeLehmann?**
###  Hoy, gracias a este descubrimiento, la discontinuidad entre el núcleo interno y el núcleo externo lleva el nombre de "discontinuidad de Lehmann".[^1]

### Como dato curioso hay una ***discontinuidad sísmica*** que lleva el mismo nombre, a unos 220 km de profundidad en el manto superior, en donde las ondas sísmicas procedentes de la corteza se aceleran cuando la atraviesan. Su presencia se detecta bajo los continentes pero no bajo los océanos.[^2]

## **#8M2021**
## **#SeismologyIsTheWave**
## **#LaSismologíaEsLaOnda**

![Inge Lehmann](/images/div/IngeLehmann.jpg)

# Fuentes: 
[^1]: Tarbuck, E. J. & Lutgens, F. K. 2005. Ciencias de la Tierra, 8ª edición. Pearson Educación S. A., Madrid
[^2]: [Deformation of Earth Materials An Introduction to the Rheology of Solid Earth](https://www.cambridge.org/core/books/deformation-of-earth-materials/3A8616AB0C5E808F605E56ABE36ED198)