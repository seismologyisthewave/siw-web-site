+++
author = "SIW-Crew"
title = "Cazadores de sismos"
date ="2022-02-10"
tags = [ "Cazadores Sismos", "Divulgación", "Sismología"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1_Xd4Ms6KK7R1QQGhVGbzTGWBMdaJye3w"
+++

# Cazadores de sismos

### Para el equipo de Seismology is the wave es un gusto compartirles esta bonita y educativa historia. Las y los protagonistas de la misma accedieron a participar en el comic. 
##### Agradecemos a *Abel Jesús Adaya González*, *Caridad Cárdenas Monroy*, *Xyoli Pérez-Campos* y *Betty Zanolli Fabila* por su apoyo en la elaboración y proceso de publicación de este contenido.

##### ¡De parte de todas y todos esperamos que lo disfruten y no olviden compartirlo!


<!-- QUE NO LE MUEVAS PUÑETAS
 -->

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>
</head>
<body>

<!-- Aquí se agregan las imagenes lo demás ni le muevas wey -->

<div class="slideshow-container">

<div class="mySlides fade">
  <div class="numbertext">1 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1_Xd4Ms6KK7R1QQGhVGbzTGWBMdaJye3w" style="width:100%">
  <div class="text">Página 1</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">2 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1Vcxx-Njs8UZa4yc3BFeGFrW8iKq3jThN" style="width:100%">
  <div class="text">Página 2</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">3 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1yGFhZQtu3gOaesDk5V83SqozlcN7Obdt" style="width:100%">
  <div class="text">Página 3</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">4 / 23</div>
  <img src="https://drive.google.com/uc?export=view&id=1E8b2L5_q3_9tbQp2cvovHrzCeHAsXO6I" style="width:100%">
  <div class="text">Página 4</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">5 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1s-kV1ts6NfJybkDoD-k40azasE-Y4GE6" style="width:100%">
  <div class="text">Página 5</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">6 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1a0tnhwChBDk80ytFMzkMgzNpA9yCrmSp" style="width:100%">
  <div class="text">Página 6</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">7 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1wtlTzBnuWALhe4VlHPwqAcSIqjDEfaor" style="width:100%">
  <div class="text">Página 7</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">8 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=18oqUCW_oYXhzp11iTbb2DQr_08UW-c9k" style="width:100%">
  <div class="text">Página 8</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">9 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1xuGtDDK6FT01FXnc6xQdf2xzYDEAndKi" style="width:100%">
  <div class="text">Página 9</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">10 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1M0lPEOZJurJkG_2fzFkD1xjEVipk2NAy" style="width:100%">
  <div class="text">Página 10</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">11 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1oV7WrDz7gvJcBoODM8_0rkBsPaOwACFR" style="width:100%">
  <div class="text">Página 11</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">12 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1E1wnejoe4-ywfAxpGN41xX5g5utYyrv_" style="width:100%">
  <div class="text">Página 12</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">13 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1weIM8KmrJA4kI0buGw3zaVULffyD_NEO" style="width:100%">
  <div class="text">Página 13</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">14 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1yjutesC_a71Uq7mB6pzZnk9F68T5eKQg" style="width:100%">
  <div class="text">Página 14</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">15 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1tFm6xDZFGWZil4OM9pMRMHavvy28UzJ2" style="width:100%">
  <div class="text">Página 15</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">16 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1O6vVGOHXgSXqbGYisTubqVcMbunDM0em" style="width:100%">
  <div class="text">Página 16</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">17 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1wSkgrF547775L5WqITwWZR0hjXtSei87" style="width:100%">
  <div class="text">Página 17</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">18 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1YooQCJnTOi_f3C_m7MzB_VNlCSpc6sp-" style="width:100%">
  <div class="text">Página 18</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">19 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1YIx509-6QWMqSkIqe4aN2uqrz4HYIK-Q" style="width:100%">
  <div class="text">Página 19</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">20 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1R2ZvcxjDZKzp_xVbaQQeGEwNh7ELLAr8" style="width:100%">
  <div class="text">Página 20</div>
</div>

<div class="mySlides fade">
  <div class="numbertext">21 / 21</div>
  <img src="https://drive.google.com/uc?export=view&id=1luFyaArph_cgyE-JGA4fGNCi8ds5AQOj" style="width:100%">
  <div class="text">Página 21</div>
</div>


<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

</body>
</html> 


### *#SeismologyIsTheWave*
### *#LaSismologíaEsLaOnda* 
