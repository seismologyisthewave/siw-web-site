+++
author = "SIW-Crew"
title = "El sismo del Ángel, 1957"
date ="2021-07-28"
tags = [ "Sismo Histórico", "Sismología"] 
thumbnail = "images/div/ángel1957_2.jpg"
+++

### **#SismicidadHistórica**
##### El 28 de julio de 1957 ocurrió un sismo con magnitud 7.8 al NE de San Marcos, Guerrero cerca de las costas de Acapulco. Este sismo ocasionó daños en la **#CDMX**, incluso provocó que el **#AngelDeLaIndependencia** cayera de su pedestal; por esta razón, se le conoce como *"el sismo del Ángel"*.

<img src="/images/div/ángel1957_1.jpg" alt="Sismo del Ángel, 1957." width="640" height="360"> [^1]
<img src="/images/div/ángel1957_2.jpg" alt="Sismo del Ángel, 1957." width="640" height="360"> [^1]
<img src="/images/div/ángel1957_3.jpg" alt="Sismo del Ángel, 1957." width="640" height="360"> [^1]




### **#SeismologyIsTheWave**  

### **#LaSismologíaEsLaOnda**


[^1]: Fotografías tomadas del [archivo del Gobierno de la CDMX](https://www.gob.mx/cenapred/articulos/aniversario-63-del-sismo-del-angel-de-1957)
