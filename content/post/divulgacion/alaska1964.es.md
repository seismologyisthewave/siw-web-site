+++
author = "SIW-Crew"
title = "Gran sismo de Alaska 28 de marzo de 1964"
date ="2021-03-28"
tags = [ "Sismo Histórico", "Sismología"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1AsYJPLVDqLwk_5jDmz31FybpEJuRH0jl"
+++

### **#SismicidadHistórica**
##### El sábado 28 de marzo de 1964 ocurrió un sismo conocido como “el gran sismo de Alaska”, M=9.2,[^1] en la región de Prince William Sound de Alaska.

<img src="https://drive.google.com/uc?export=view&id=1AsYJPLVDqLwk_5jDmz31FybpEJuRH0jl" alt="Imagen modificada de Brocher, et al, 2014" width="640" height="360"> [^2]

##### Este sismo es el más grande registrado en Norteamérica y el segundo más grande del cual se tiene un registro histórico en todo el mundo.
##### El sismo provocó un tsunami que impactó la costa oeste de Canadá y Estados Unidos. La ola más grande tuvo una altura de 67m y se registro en Shoup Bay, Port Valdez.[^3]

<iframe width="640" height="360" src="https://www.youtube.com/embed/Lac4Zs_CIdw" alt="Animación del Tsunami de la NOAA, 2021" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### Aunque el gran sismo de Alaska fue trágico, debido a la pérdida de vidas y daños materiales, proporcionó una gran cantidad de datos e información sobre los sismos que ocurren en esa zona de subducción y los peligros que representa.

### **#SeismologyIsTheWave**  

### **LaSismologíaEsLaOnda**


[^1]:([USGS](https://www.usgs.gov/), 2006)
[^2]:(Brocher, et el, 2014, https://pubs.usgs.gov/fs/2014/3018/pdf/fs2014-3018.pdf) 
[^3]:([NOAA](https://nctr.pmel.noaa.gov/alaska19640328/), 2021)
