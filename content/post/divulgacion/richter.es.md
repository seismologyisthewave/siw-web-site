+++
author = "SIW-Crew"
title = "La escala de Richter"
date ="2021-07-07"
tags = [ "Richter", "Sismología", "Richter Sacale" ] 
thumbnail = "images/div/Richter_Scale.jpg"
+++


# La escala de Richter
## ¿Qué onda con la escala de Richter?

##### Sin duda todos han escuchado sobre la escala de Richter (**así, a secas; es decir, sin grados)**. Hoy te platicaremos qué es y cómo fue diseñada.

#### La escala de magnitud local **(ML)** o escala de Richter es la escala más antigua y famosa para estimar la magnitud de un sismo. Fue diseñada por **Charles Richter** y **Beno Gutemberg** en 1935 al medir el tamaño de sismos que ocurrían al sur de California.  
#### Richter y Gutemberg usaron datos con un contenido de frecuencias relativamente alto, en estaciones sísmicas cercanas. [^1]
#### La escala de magnitud ML fue diseñada para cuantificar el tamaño de sismos de pequeños a medianos, pero no para aquellos sismos grandes y lejanos. Para estimar ML, Richter y Gutemberg usaron sismogramas registrados en un sismógrafo de torsión llamado Wood-Andreson. [^2]
#### La escala de magnitud ML depende de la amplitud máxima medida en el sismograma y la distancia que existe desde donde está el instrumento hasta el evento. Todas las escalas de magnitud basadas en amplitud dependen del algoritmo base 10.  

![Richeter Scale](/images/div/Richter_Scale.jpg)


#### Esto se debe a que hay muchos factores de diferencia entre las amplitudes más pequeñas y más grandes del movimiento del terreno observado.  
#### Por ejemplo, un sismo de magnitud 5.0 en esta escala de magnitud local tiene una amplitud 10 veces mayor y corresponde a una liberación de energía 31.6 veces mayor que uno que mide 4.0 en esta misma escala. [^2]  
#### Hoy en día se usan instrumentos más modernos ( electromagnéticos). Por lo tanto, para calcular la magnitud ML se remueve la respuesta del instrumento y se añade otra intentado imitar lo que habría reproducido el sismógrafo Wood-Anderson. [^2]  
#### En muchas agencias sismológicas usan una escala de magnitud ML modificada, es decir, adaptando las propiedades del suelo en un modelo de velocidades regional para obtener la distancia.  
#### En general ***(spoiler alert!)***, la escala ML se **"satura"** lo que la hace poco confiable para sismos mayores de 5.8. 
#### Si te gusto dale 💓 y compartenos 🔁   
***(Lo puedes compartir en tus redes con los links al inicio del post)***

## Espera nuestros futuros posts  


[^1]: [USGS, 2021](https://t.co/GJF7EVrO8n?amp=1)
[^2]: [PNSN, 2021](https://t.co/7cPGsTZsil?amp=1)