+++
author = "SIW-Crew"
title = "Sismo de Tohoku, 11-03-2011"
date = "2021-03-11"
tags = [ "Sismo Histórico", "Sismología"] 
thumbnail = "images/div/11Marzo_Tohoku.png"
+++

# Sismo de Tohoku, 11-03-2011
### Hoy en **#SismicidadHistórica**

##### El 11 de marzo del 2011 a las 5:46 a.m. (UTC) ocurrió el sismo de Tohoku, Japón con magnitud Mw 9.1. Este ocurrió cerca de la costa noreste de Honshu, en la zona de subducción entre las placas del Pacífico y Norteamérica. 
![Mapa de localización y Mecanismo Focal](/images/div/11Marzo_Tohoku.png)
##### El deslizamiento entre las placas fue 50m - 60m de longitud, en un área de aproximadamente 400km x150 km.
##### Fue localizado a una profundidad de 25km, por lo que se considera un sismo somero.[^1]

---

##### Además, fue capaz de generar un tsunami; menos de una hora después de la ocurrencia del sismo, la primera ola (de un conjunto de varias más) llegó a las costas de Japón. La ola mayor tuvo una altura registrada de ~38m en Miyato, Japón. Hubo muchos daños en edificios, puentes, instalaciones eléctricas, de gas, etc., a causa del sismo y tsunami.  
<figure class="video_container">
  <video controls="true" allowfullscreen="true" >
    <source src="/images/div/20110311Houshu.mp4" type="video/mp4">
  </video>
</figure>   

[^2]  

##### Desafortunadamente al menos 15,703 personas murieron, 5,314 resultaron heridas y 130,927 fueron desplazadas a causa del tsunami. 
##### La inundación generada por el tsunami causó que fallaran los sistemas de enfriamiento en la planta nuclear de Fukushima Daiichi, lo cual resultó en un accidente nuclear.  
![Foto de Associated Press](/images/div/press_tsunami.jpg)[^3]
### **#SeismologyIsTheWave**
### **#LaSismologíaEsLaOnda**

#### Créditos Referencias y más información del evento en:

Karstens, R. and Trabant, C. (2011) DMS Data Products for the Tohoku, Japan Earthquake, IRIS Data Services Newsletter, 13, 1.

[^1]: https://earthquake.usgs.gov/earthquakes/eventpage/official20110311054624120_30/executive
[^1]: https://earthquake.usgs.gov/earthquakes/eventpage/official20110311054624120_30/impact
[^2]: Animación tomada de: (https://nctr.pmel.noaa.gov/honshu20110311/20110311Houshu.mov)
[^3]: Fotografía tomada de Associated Press. (http://www.apimages.com/collection/landing/japan-2011-tohoku-earthquake-/b2fd9f6e0b1e4d2d9c9537e7b890a3fb)


