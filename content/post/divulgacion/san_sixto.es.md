+++
author = "SIW-Crew"
title = "Sismo de San Sixto 28 de marzo de 1787"
date ="2021-03-28"
tags = [ "Sismo Histórico", "Sismología"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1sDTqjP4iDeMBfodoCyaumUeQnzkPm4ia"
+++

### **#SismicidadHistórica**
##### Uno de los sismos más grandes, del cual se tiene registro anecdótico en México, es el sismo del 28 de marzo de 1787, día de San Sixto, en lo que era el Virreinato de la Nueva España.
##### Este sismo ocurrió alrededor de las 11:30 de la mañana (hora local) y se localizó frente a las costas de Guerrero-Oaxaca. La magnitud se ha estimado entre 8.4 y 8.6. [^1]
#### El sismo ocasionó fuertes daños en la intendencia de Antequera (ahora Oaxaca) y en la, ahora, Ciudad de México, con varias edificaciones destruidas o dañadas [Suarez, 1986]. 
##### Las numerosas réplicas generadas se sintieron en Oaxaca y en la Ciudad de México durante los días posteriores y hasta el 3 de abril.[^2]
<img src="https://drive.google.com/uc?export=view&id=1sDTqjP4iDeMBfodoCyaumUeQnzkPm4ia" alt="Mapa de localización estimada" width="640" height="360">

##### Sin embargo, el efecto más impresionante de este sismo fue el tsunami que arrasó las costas del Pacífico, en la frontera entre los estados de Guerrero y Oaxaca , con olas de hasta 20m, y mar entrando por las costas por aproximadamente 8km.[^2]

##### Como dato curioso, se propuso un sismo del tipo interplaca, de la misma magnitud, en una zona cercana al del 28 de marzo de 1787 como escenario hipotético para el simulacro del 19 de septiembre de 2019.
<img src="https://drive.google.com/uc?export=view&id=1ggNnWv4Hx3ELSj8A1HKwh8VKoHlWnsGc" alt="Mapa de intensidades" width="640" height="360">

###### Mapa de intensidades para el sismo hipotético de M8.6 creado por el Instituto de Ingeniería. [^3]

##### Aunque el gran sismo de Alaska fue trágico, debido a la pérdida de vidas y daños materiales, proporcionó una gran cantidad de datos e información sobre los sismos que ocurren en esa zona de subducción y los peligros que representa.

### **#SeismologyIsTheWave**  

### **LaSismologíaEsLaOnda**


[^1]:[Suarez y Albini, 2009]
[^2]:[Suarez, 1986] [De sismos históricos mexicanos: Los temblores del 28 de marzo al 3 de abril de 1787. Gerardo Suárez R.](https://www.ugm.org.mx/publicaciones/geos/pdf/geos86-4/sismos-6-4.pdf)
[^3]:[Reporte especial SSN, 2019](http://www.ssn.unam.mx/sismicidad/reportes-especiales/2019/SSNMX_simulacro_20190919_Oaxaca-Guerrero_M86.pdf)