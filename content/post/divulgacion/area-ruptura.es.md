+++
author = "SIW-Crew"
title = "Área de Ruptura"
date ="2021-01-19"
tags = [ "Área de ruptura", "Sismología"] 
thumbnail = "images/div/AreaRuptura-1.png"
+++

### Hoy les vamos a platicar qué es el área de ruptura 🤓.


#### * Las placas tectónicas suelen moverse lentamente y en algunos lugares se quedan atoradas debido a la fricción. Dado que éstas no dejan de moverse se va acumulando energía con el paso de tiempo. Un sismo es originado por un deslizamiento repentino de estos bloques de roca sobre un plano, generalmente llamado plano de falla, y sucede cuando el esfuerzo acumulado es mayor que la fricción que detiene las placas. Esto produce una liberación súbita de energía, generando ondas sísmicas que viajan a través de la Tierra [^1]. 
#### * Por simplicidad, cuando ocurre un sismo se reporta su epicentro, que es la proyección en superficie del punto donde “empieza” el sismo, o también llamado hipocentro.
#### * Cuando ocurre un sismo pequeño el ÁREA DE INTERACCIÓN entre las placas es pequeña y por eso esta aproximación puntual funciona muy bien. Sin embargo, con los sismos de gran magnitud esta aproximación no es tan a adecuada, pues lo que en realidad ocurre es que una región más grande de la placa se mueve con respecto de la otra, lo cual recibe el nombre de ÁREA DE RUPTURA. Esta a su vez *#SpoilerAlert*, es determinante para estimar la magnitud de un sismo o su tamaño (pero esa es otra historia que les contaremos en el futuro).      

![Área de ruptura](/images/div/AreaRuptura-1.png "Ejemplo de un área de ruptura")

# Fuentes:

[^1]: [USGS](https://www.usgs.gov/)

# **#SeismologyIsTheWave**
# **#LaSismologíaEsLaOnda**