+++
author = "SIW-Crew"
title = "Mecanismos focales"
date ="2021-04-21"
tags = [ "Mecanismo Focal", "Sismología" ] 
thumbnail = "https://drive.google.com/uc?export=view&id=1JhbHwttSeQz6Dvkwo8dp4TqX51f3GaWO"
+++


## A continuación mostraremos los tres tipos de mecanismos focales típicos en sismología con 3 figuras.

### La Figura 1, muestra un mecanismo tipo inverso.  
<img src="https://drive.google.com/uc?export=view&id=1JhbHwttSeQz6Dvkwo8dp4TqX51f3GaWO" alt="Inverso" width="640" height="360">


### **a)** Semiesfera inferior de, lo que en el argot sismológico se llama, **mecanismo focal.** En él, se muestra el modelo de ruptura de un sismo de ***tipo inverso***. 

### **b)** Vista en planta de **a)**. Este tipo de figuras **(pelotas de playa)** se pueden utilizar para determinar el tipo de fallamiento de un sismo. Como el movimiento sobre el plano de falla ocurre hacia arriba, el color oscuro **(compresión)** queda rodeado por el color claro **(dilatación)**.

### **c)** Representación del fallamiento con bloques. Como uno sube con respecto al otro **(el de la derecha)**, esto resulta en un evento que llamamos de tipo inverso.

### **d)** Ejemplo de un mecanismo focal real de este tipo, correspondiente al sismo ocurrido en México la mañana del 19 de septiembre de 1985. [^1]


## La figura 2 muestra un mecanismo de tipo normal  
<img src="https://drive.google.com/uc?export=view&id=1zOQ9cNfNahYbs9TPmaLuybmCLamV_XHo" alt="Normal" width="640" height="360">

### **a)** Semiesfera inferior de, lo que en el argot sismológico se llama, mecanismo focal. En él, se muestra el modelo de ruptura de un sismo de ***tipo normal***.
### **b)** Vista en planta de **a)**. Este tipo de figuras **(pelotas de playa)** se pueden utilizar para determinar el tipo de fallamiento de un sismo. Como el movimiento sobre el plano de falla ocurre hacia abajo, el color oscuro **(compresión)** queda rodeando al color claro **(dilatación)**.
### **c)** Representación del fallamiento con bloques. Uno cae con respecto al otro **(el de la derecha)** y eso resulta en un evento que llamamos de tipo normal.
### **d)** Ejemplo de un mecanismo focal real de este tipo, correspondiente al sismo ocurrido en territorio mexicano el 19 de septiembre de 2017. [^1]


## La figura 3 muestra un mecanismo de corrimiento lateral
<img src="https://drive.google.com/uc?export=view&id=1z19VqSfYdt6gGxPWQLmlAP5FGX3ACC6u" alt="Corrimiento Lateral" width="640" height="360"> 

### **a)** Semiesfera inferior de lo que en el argot sismológico se llama mecanismo focal. En él, se muestra el modelo de ruptura de un sismo de ***corrimiento lateral***.  
### **b)** Vista en planta de **a)**. Este tipo de figuras **(pelotas de playa)** se pueden utilizar para determinar el tipo de fallamiento de un sismo. En este ejemplo, el movimiento sobre el plano de falla ocurre hacia abajo y está marcado por el área de color oscuro **(compresión)**. El color claro corresponde con la dilatación.
### **c)** Representación del fallamiento con bloques. Como uno se desplaza con respecto al  otro **(en este caso, el semicírculo de la izquierda hacia arriba y el de la derecha hacia abajo)**, esto resulta en un evento que llamamos de corrimiento lateral.
### **d)** Ejemplo de un mecanismo focal real de este tipo, correspondiente al sismo del 21 de octubre de 2010 ocurrido en el Golfo de California. [^1]



# Fuentes:
[^1]: [Global CMT, 2020](https://www.globalcmt.org/)
