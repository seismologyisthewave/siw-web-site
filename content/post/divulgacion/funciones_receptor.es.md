+++
author = "SIW-Crew"
title = "Funciones Receptor"
date ="2021-04-07"
tags = [ "Funciones receptor", "Sismología", ] 
thumbnail = "https://drive.google.com/uc?export=view&id=1tyCUqe60eV1DrmwIhQnX9_8nx80tlkDK"
+++

# Funciones Receptor

### Sabían que gracias a los sismos podemos estudiar cómo está constituida la Tierra (o los planetas, como Marte, o también la Luna).

### Hoy les vamos a platicar de la función de receptor. ¿Qué es y para qué sirve?
### Una función de receptor es una serie de tiempo (parece sismograma) que nos brinda información sobre la estructura relativa de la Tierra. 

<img src="https://drive.google.com/uc?export=view&id=1tyCUqe60eV1DrmwIhQnX9_8nx80tlkDK" alt="FR" width="640" height="360"> 

### Se obtiene a partir de los sismogramas de eventos muy lejanos, que llamamos telesismos, registrados en una estación.

<img src="https://drive.google.com/uc?export=view&id=1CznXMtpNnq2A9b76pcJrQSQHVajgN92G"  width="640" height="360">

### Para saber un poco más de ella, podemos pensar que un sismograma contiene la contribución de: 

1. La fuente que originó el sismo.
1. La forma en la que el instrumento registra un sismo (que es muy particular en cada sismómetro).
1. La estructura de la Tierra.

### ¿Por qué tienen que ser sismos muy lejanos? 🤔

### El método parte del supuesto que, teniendo los sismogramas de las componentes vertical y dos horizontales, en la vertical sólo tenemos la contribución de la fuente y el instrumento.
 
<iframe width="640" height="360" src="https://www.iris.edu/hq/inclass/uploads/videos/A_006_seismicbuilding_1station1seismogram.mp4" alt="Registro de un TelesismoIRIS" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

[^1]

### Ahora, si dividimos (ojo, en el dominio de las frecuencias) una de las componentes horizontales entre la vertical y simplificamos ["¡despacio cerebrito!" - No te preocupes, digamos que quitamos lo que es igual], al final nos queda información de la estructura de la Tierra.
### Esta señal, sí la que parece un sismograma muy simple, está construida por pulsos de las ondas que se transformaron de P a S dado un cambio en las propiedades elásticas del medio a través del cual viajó.
### Ya para finalizar, a partir de estos pulsos, que llegan en cierto tiempo, podemos obtener distancias o profundidades, si es que conocemos o suponemos una velocidad.
### Y es tan fácil[^2] como aplicar la ecuación:
	d = vt [distancia = velocidad x tiempo]

#### Si te gustó esta información no olvides darnos 💓 y compartirnos 🔁. Si te surgieron más dudas también puedes contactarnos y con gusto te confundimos más 🤭.

## **#SeismologyIsTheWave** 
## **#LaSismologíaEsLaOnda.** 

# Fuentes

[^1]: Tomada de [iris.edu](https://www.iris.edu/hq/inclass/animation/1component_seismogram_building_responds_to_p_s_surface_waves)
[^2]:Bueno, no suele ser tan simple pero más o menos es así.