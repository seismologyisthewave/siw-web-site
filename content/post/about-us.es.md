+++
author = "SIW-Crew"
title = "¿Quiénes somos?"
date = "2020-04-17"
tags = [ "Sismología", "SIW"] 
thumbnail = "https://drive.google.com/uc?export=view&id=1u49W0k2Ea3XrAlA2r1u9Tw4aFneDZdZb"

+++
## Somos un grupo de estudiantes que pensamos que la sismología y su lenguaje no debe de ser ajeno al público. En este espacio, queremos explicar de manera sencilla y con humor algunos de los temas principales de la sismología

## *Integrantes*

+ ### Diego V. I. Espinosa Kuswara
|<img src="/images/fotos/DiegoC.png" alt="Diego" width="1000" height="1000"> | <ul><li>Estudiante de décimo semestre de Estudiante de décimo semestre de Ingeniería Geofísica en la Facultad de Ingeniería. Sus campos de interés son la sismología y geofísica espacial. De este primer campo de interés, le gustaría trabajar en el área de sismogeodesia.</li><ul>|
|---|--|

+ ### María Jimena Vega Báez
| <ul><li> Ingeniera Geofísica, egresada de la Facultad de Ingeniería de la Universidad Nacional Autónoma de México (UNAM). Actualmente cursa el tercer semestre de la maestría en Ciencias de la Tierra en la UNAM.  En la licenciatura inició en el campo de la sismología analizando sismicidad en el estado de Hidalgo, actualmente trabaja en tomografía de sismicidad local especialmente en el Valle de México para el análisis de su estructura cortical.</li></ul>| <img src="/images/fotos/Jimena.png" alt="Jimena" width="1280" height="1280">|
|---|---|

+ ### Manuel J. Aguilar-Velázquez
|<img src="/images/fotos/Manuel.png" alt="Manu" width="2000" height="2000"> |<ul><li> Es egresado de la carrera de Ingeniería Geofísica por la Facultad de Ingeniería, UNAM. Estudió la Maestría en Ciencias también en la UNAM y actualmente estudia el Doctorado en Ciencias de la Tierra. Desde 2016 se desempeña como ayudante de profesor en la Facultad de Ingeniería. Formó parte del Grupo de Análisis del Servicio Sismológico Nacional entre 2016 y 2018. Su campo de interés es la sismología, concretamente el conocer la estructura de la Tierra. Actualmente estudia la estructura cortical y el comportamiento de las ondas sísmicas en el Valle de México.</li><ul>|
|--|--|


+ ### Leonarda I. Esquivel-Mendiola
| <ul><li>Leonarda Esquivel Mendiola egresó de la Facultad de Ingeniería de la UNAM como Ingeniera Geofísica. Fue becaria en el Servicio Sismológico Nacional. Entre 2018-2020 estudió la maestría en Ciencias de la Tierra en la UNAM. Actualmente cursa el tercer semestre de doctorado en Ciencias de la Tierra enfocado en sismología. Sus campos de conocimiento y de interés son la sismología, vulcanología y energía geotérmica. Sus áreas de estudio son tomografía sísmica, redes de monitoreo sísmico y ruido sísmico.</li><ul>| <img src="/images/fotos/Leonarda_Esquivel.png" alt="Leo" width="1280" height="1280">
|---|--|



+ ### Luis D. Martínez-Jiménez  
|<img src="/images/fotos/Luis.png" alt="Luis" width="1280" height="1280">| <ul><li> Ingeniero Geofísico, egresado en 2018 de la División Académica de Ciencias Básicas de la Universidad Juárez Autónoma de Tabasco (UJAT). Fue becario del Servicio Sismológico Nacional de la UNAM por unos meses, después se convirtió en analista de tiempo completo en el mismo lugar. Líneas de trabajo: monitoreo de datos en tiempo real, obtención de parámetros para sismos locales y regionales.  Actualmente trabaja para la industria.  Tiene una atracción por la sismología, procesado de datos y los idiomas. Le gustan los videojuegos, la música y los libros de ciencia ficción. </li><ul>|  
|---|--|


+ ### [Raúl Daniel Corona-Fernández](https://rdcorona.gitlab.io/rdcorona-site)
| <ul><li> Soy ingeniero Geofísico egresado de la Facultad de Ingeniería de la UNAM en 2013, con 3 años de experiencia en ejecución y coordinación de proyectos de exploración petrolera y del subsuelo.  De 2015-2017 cursé la maestría en Ciencias de la Tierra de la UNAM, en la Escuela Nacional de Estudios Superiores (ENES) campus Morelia.  En 2018 comencé el doctorado en Ciencias de la Tierra en el Instituto de Geofísica de la UNAM.  Linea de trabajo es la Sismología histórica y fuente sísmica.  Sismólogo con más de 5 años de experiencia en el procesamiento de datos para el análisis de grandes sismos históricos en México. Ha desarrollado recursos humanos en apoyo a la docencia y es creador de contenido de divulgación científica.  Me gusta leer (novelas y libros de aventura y ciencia ficción), hacer ejercicio, andar en bicicleta, y escuchar música todo el tiempo.</li></ul>| <img src="/images/fotos/RaulC.png" alt="Daniel" width="2000" height="2000">|
|---|---|

+ ### Miguel Rodríguez-Domínguez
|<img src="/images/fotos/Miguel.png" alt="Mike" width="1280" height="1280">|<ul><li> Soy egresado de la carrera de Ingeniería Geofísica por la Facultad de Ingeniería de la UNAM, maestro en Ciencias de la Tierra por el Posgrado en Ciencias de la Tierra del Instituto de Geofísica de la UNAM y actualmente estoy estudiando el doctorado en Ciencias en el mismo instituto. Mi línea de investigación está enfocada en el estudio de la estructura sísmica en el centro-sur de México. He colaborado con la Unión Geofísica Mexicana, como parte del equipo técnico, y actualmente soy parte del grupo de Análisis del Servicio Sismológico Nacional. Me gusta el café y disfruto un buen libro de ciencia-ficción o fantasía. </li></ul>|
|---|--|


---
---
